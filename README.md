## Cài đặt dự án

### b1: clone dự dán
````
git clone https://gitlab.com/Hoang2k/room-management.git
````

### b2 - env
````
cp .env.example .env
````
### b3 - cài đặt composer và npm
````
composer install
npm install
````

### b4: tạo key
````
php artisan key:generate
````
#### b5: tạo các bảng
````
sh sh/migration.sh
````
### b6:  Xóa cache
``````
php artisan cache:clear
php artisan config:cache
php artisan config:clear
``````

<?php

namespace App\Jobs;

use App\Mail\MailNotify;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Modules\Manage\Events\LogHistory;

class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $email;
    protected $total_debt;

    public function __construct($email, $total_debt)
    {
        $this->email = $email;
        $this->total_debt = $total_debt;
    }


    public function handle()
    {
        $email = new MailNotify($this->total_debt);
        Mail::to($this->email)->send($email);
        event(new LogHistory($this->email,'Send Mail'));

    }
}

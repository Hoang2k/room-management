<?php

namespace App\Console\Commands;

use App\Jobs\SendEmail;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Modules\Manage\Services\BillService;

class SendMailsCommand extends Command
{

    protected $signature = 'emails:send';
    protected $billService;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command send mail';

    public function __construct(
        BillService $billService
    )
    {
        $this->billService = $billService;
        parent::__construct();
    }


    public function handle()
    {
        $customer_debts = $this->billService->getCustomerDebt();

        foreach ($customer_debts as $customer_debt) {

            $email = $customer_debt->Tenant->email;
            $total_debt = $customer_debt->total_debt;
            $emailJob = (new      SendEmail($email, $total_debt))->delay(Carbon::now()->addMinutes(1));
            dispatch($emailJob);
        }
    }
}

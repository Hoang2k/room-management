<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailNotify extends Mailable
{
    use Queueable, SerializesModels;

    protected $total_debt;

    public function __construct($total_debt)
    {
        $this->total_debt = $total_debt;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('dinhvanhoang472000@gmail.com')
            ->subject('Thông báo tiền trọ')
            ->view('mails.index', [
                'total_debt' => $this->total_debt
            ]);
    }
}

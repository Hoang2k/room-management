<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenantContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_contracts', function (Blueprint $table) {
            $table->id();
            $table->integer('apartment_room_id');
            $table->string('tenant_id');
            $table->integer('pay_period');
            $table->bigInteger('price');
            $table->Integer('electricity_pay_type');
            $table->bigInteger('electricity_price');
            $table->Integer('electricity_number_start');
            $table->Integer('water_pay_type');
            $table->bigInteger('water_price');
            $table->Integer('water_number_start');
            $table->integer('number_of_tenant_current');
            $table->text('note')->nullable();
            $table->dateTime('start_time');
            $table->dateTime('end_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenant_contracts');
    }
}

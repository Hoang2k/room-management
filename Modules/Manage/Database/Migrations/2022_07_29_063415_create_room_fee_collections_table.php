<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomFeeCollectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_fee_collections', function (Blueprint $table) {
            $table->id();
            $table->integer('apartment_id');
            $table->integer('apartment_room_id');
            $table->integer('tenant_id');
            $table->integer('electricity_number');
            $table->string('electricity_image')->comment('Ảnh đồng hồ điện')->nullable();
            $table->string('water_number');
            $table->string('water_image')->comment('Ảnh đồng hồ nước')->nullable();
            $table->date('charge_date')->comment('Ngày tính tiền');
            $table->bigInteger('total_price')->nullable();
            $table->bigInteger('total_paid');
            $table->bigInteger('total_debt')->comment('Tổng nợ')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room_fee_collections');
    }
}

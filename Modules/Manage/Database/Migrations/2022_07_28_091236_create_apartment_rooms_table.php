<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApartmentRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apartment_rooms', function (Blueprint $table) {
            $table->id();
            $table->integer('apartment_id');
            $table->integer('room_price')->nullable();
            $table->integer('room_number');
            $table->bigInteger('max_tenant')->nullable();
            $table->string('image')->nullable();
            $table->tinyInteger('status');
            $table->text('meta')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apartment_rooms');
    }
}

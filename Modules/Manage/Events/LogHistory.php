<?php

namespace Modules\Manage\Events;

use Illuminate\Queue\SerializesModels;

class LogHistory
{
    use SerializesModels;

    public $data;
    public $action;
    public $apartment_id;

    public function __construct($data, $action)
    {
        $this->data = $data;
        $this->action = $action;

    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}

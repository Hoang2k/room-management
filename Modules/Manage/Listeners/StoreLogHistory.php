<?php

namespace Modules\Manage\Listeners;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Manage\Entities\Activity;
use Modules\Manage\Events\LogHistory;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class StoreLogHistory
{
   protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Handle the event.
     *
     * @param LogHistory $event
     * @return void
     */
    public function handle(LogHistory $event)
    {
        $data['user_id']      = Auth::id();
        $data['action']       = $event->action;
        $data['route']        = $this->request->path();
        $data['data']         = json_encode($event->data);
        $data['created_at']   = now();
        $this->__createAuditLog($data);

    }

    //Ghi lại dữ liệu Log
    private function __createAuditLog($data)
    {
        Activity::insert($data);
    }

}

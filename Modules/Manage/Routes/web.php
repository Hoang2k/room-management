<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group([
    'middleware' => ['web', 'auth'],
], function () {
    Route::prefix('manage')->group(function () {
        Route::get('/', 'ApartmentController@index')->name('get.apartment.index');
        Route::get('/create', 'ApartmentController@create')->name('get.apartment.create');
        Route::post('/store', 'ApartmentController@store')->name('get.apartment.store');
        Route::get('/edit/{id}', 'ApartmentController@edit')->name('get.apartment.edit');
        Route::post('/update/{id}', 'ApartmentController@update')->name('get.apartment.update');
        Route::get('/delete/{id}', 'ApartmentController@destroy')->name('get.apartment.destroy');

        Route::prefix('room')->group(function () {
            Route::get('/', 'RoomController@index')->name('get.room.index');
            Route::get('/create', 'RoomController@create')->name('get.room.create');
            Route::post('/store', 'RoomController@store')->name('get.room.store');
            Route::get('/edit/{id}', 'RoomController@edit')->name('get.room.edit');
            Route::post('/update/{id}', 'RoomController@update')->name('get.room.update');
            Route::get('/delete/{id}', 'RoomController@destroy')->name('get.room.destroy');
        });
        Route::prefix('tenants')->group(function () {
            Route::get('/', 'TenantController@index')->name('get.tenant.index');
            Route::get('/create', 'TenantController@create')->name('get.tenant.create');
            Route::post('/store', 'TenantController@store')->name('get.tenant.store');
            Route::get('/edit/{id}', 'TenantController@edit')->name('get.tenant.edit');
            Route::post('/update/{id}', 'TenantController@update')->name('get.tenant.update');
            Route::get('/delete/{id}', 'tenantController@destroy')->name('get.tenant.destroy');
        });
        #Upload
        Route::post('upload/services', 'UploadController@store');

        //Tính tiền trọ hàng tháng

        Route::prefix('bills')->group(function () {
            Route::get('/', 'BillController@index')->name('get.bill.index');
            Route::get('/create', 'BillController@create')->name('get.bill.create');
            Route::post('/store', 'BillController@store')->name('get.bill.store');
            Route::get('/edit/{id}', 'BillController@edit')->name('get.bill.edit');
            Route::post('/update/{id}', 'BillController@update')->name('get.bill.update');
            Route::get('/delete/{id}', 'BillController@destroy')->name('get.bill.destroy');
        });
        Route::get('/statistical/chart', 'StatisticalController@index')->name('get.statistical.chart');
        Route::get('/data/chart', 'StatisticalController@getDataForChart')->name('get.data.chart');

        //Dịch vụ

        Route::prefix('monthly_costs')->group(function () {
            Route::get('/', 'MonthlyCostController@index')->name('monthly.cost.index');
            Route::get('/create', 'MonthlyCostController@create')->name('monthly.cost.create');
            Route::post('/store', 'MonthlyCostController@store')->name('monthly.cost.store');
            Route::get('/edit/{id}', 'MonthlyCostController@edit')->name('monthly.cost.edit');
            Route::post('/update/{id}', 'MonthlyCostController@update')->name('monthly.cost.update');
            Route::get('/delete/{id}', 'MonthlyCostController@destroy')->name('monthly.cost.destroy');
        });
        // Hợp đồng thuê nhà
        Route::prefix('tenant-contact')->group(function () {
            Route::get('/', 'TenantContractController@index')->name('tenant_contract.index');
            Route::get('/create/{id}', 'TenantContractController@create')->name('tenant_contract.create');
            Route::post('/store', 'TenantContractController@storeOrUpdate')->name('tenant_contract.store');
            Route::get('/edit/{id}', 'TenantContractController@edit')->name('tenant_contract.edit');
            Route::post('/update/{id}', 'TenantContractController@update')->name('tenant_contract.update');
            Route::get('/delete/{id}', 'TenantContractController@destroy')->name('tenant_contract.destroy');
        });
        //user
        Route::prefix('user')->group(function () {
            Route::get('/', 'AdminController@getUser')->name('get.user.index');
            Route::get('/chart', 'AdminController@getChart')->name('get.chart.index');
            Route::get('data/chart', 'AdminController@getDataForChart');

        });
    });

});


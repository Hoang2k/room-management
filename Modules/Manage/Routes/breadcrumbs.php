<?php

\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('apartment', function ($trail) {
    $trail->push('Danh sách tòa nhà', route('get.apartment.index'));
});


\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('apartment::add', function ($trail) {
    $trail->parent('apartment');
    $trail->push('Thêm tòa nhà', route('get.apartment.create'));
});

\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('apartment::edit', function ($trail) {
    $trail->parent('apartment');
    $trail->push('Sửa tòa nhà');
});

//Room

\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('apartment_room', function ($trail) {
    $trail->push('Danh sách phòng trọ', route('get.room.index'));
});


\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('apartment_room::add', function ($trail) {
    $trail->parent('apartment_room');
    $trail->push('Thêm phòng', route('get.room.create'));
});

\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('apartment_room::edit', function ($trail) {
    $trail->parent('apartment');
    $trail->push('Sửa phòng');

});

//Tenants
\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('tenants::add',function ($trail) {
    $trail->parent('apartment_room');
    $trail->push('Thêm khách hàng');
});
\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::for('tenants::edit',function ($trail) {
    $trail->parent('apartment_room');
    $trail->push('Sửa khách hàng');
});

//Bills
Breadcrumbs::for('bills',function ($trail) {
    $trail->push('Danh sách hóa đơn',route('get.bill.index'));
});

Breadcrumbs::for('bills::add',function ($trail) {
    $trail->parent('bills');
    $trail->push('Thêm hóa đơn');
});

Breadcrumbs::for('bills::edit',function ($trail) {
    $trail->parent('bills');
    $trail->push('Sửa hóa đơn');
});

Breadcrumbs::for('customer_debt',function ($trail) {
    $trail->push('Danh sách nợ tiền phòng');
});

//Hợp đồng
Breadcrumbs::for('contract::add',function ($trail) {
    $trail->push('Thêm hợp đồng');
});

//Dịch vụ
Breadcrumbs::for('monthly_cost',function ($trail) {
    $trail->push('Danh sách dịch vụ',route('monthly.cost.index'));
});

Breadcrumbs::for('monthly_cost::add',function ($trail) {
    $trail->parent('monthly_cost');
    $trail->push('Thêm dịch vụ');
});

Breadcrumbs::for('monthly_cost::edit',function ($trail) {
    $trail->parent('monthly_cost');
    $trail->push('Sửa dịch vụ');
});

//user
Breadcrumbs::for('user',function ($trail) {
    $trail->push('Danh sách user');
});


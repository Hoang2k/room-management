<?php

namespace Modules\Manage\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RoomFeeCollection extends Model
{
    use HasFactory;

    protected $table = 'room_fee_collections';
    protected $fillable = [
        'apartment_id','apartment_room_id','tenant_id','electricity_number','water_number','charge_date','total_price','total_paid','total_debt'
    ];


    public function Apartment() {

        return $this->belongsTo(Apartment::class,'apartment_id');
    }

    public function Room() {

        return $this->belongsTo(ApartmentRoom::class,'apartment_room_id');
    }

    public function Tenant() {

        return $this->belongsTo(Tenant::class,'tenant_id');
    }
}

<?php

namespace Modules\Manage\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TenantContract extends Model
{
    use HasFactory;

    protected $table = 'tenant_contracts';
    protected $fillable = [
        'apartment_room_id', 'tenant_id', 'pay_period', 'price', 'electricity_pay_type', 'electricity_price', 'electricity_number_start', 'water_pay_type', 'water_price', 'water_number_start',
        'number_of_tenant_current', 'note', 'start_time', 'end_time'
    ];

    public function Tenant()
    {
        return $this->hasOne(Tenant::class, 'id');
    }

    public function ApartmentRoom()
    {
        return $this->belongsTo(ApartmentRoom::class,'apartment_room_id');
    }

}

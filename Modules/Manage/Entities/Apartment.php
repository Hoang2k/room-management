<?php

namespace Modules\Manage\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Apartment extends Model
{
    use HasFactory;

    protected $table = 'apartments';
    protected $fillable = ['name','image','user_id','address','description','status'];


}

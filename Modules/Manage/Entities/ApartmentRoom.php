<?php

namespace Modules\Manage\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ApartmentRoom extends Model
{
    use HasFactory;

    protected $table = 'apartment_rooms';
    protected $fillable = ['apartment_id', 'room_price', 'room_number', 'max_tenant', 'image', 'status', 'description'];


    public function Apartment()
    {

        return $this->belongsTo(Apartment::class, 'apartment_id');
    }

    public function Tenant()
    {
        return $this->hasOne(Tenant::class, 'id');
    }

    public function tenantContract()
    {
        return $this->hasOne(TenantContract::class);
    }
}

<?php

namespace Modules\Manage\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Tenant extends Model
{
    use HasFactory;

    protected $table = 'tenants';
    protected $fillable = ['name','email','phone','identity_card_number','room_id'];


    public function ApartmentRoom() {
        return $this->hasMany(Apartment::class,'room_id');
    }
}

<?php

namespace Modules\Manage\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class MonthlyCost extends Model
{
    use HasFactory;

    protected $table = 'monthly_costs';
    protected $fillable = [
        'name',
        'type_service',
        'unitPrice',
        'description'
    ];


}

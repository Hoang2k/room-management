<?php

namespace Modules\Manage\Enum;

class ApartmentStatusEnum
{

    const NO_ACTIVE = 0;
    const ACTIVE    = 1;

    public static $statusText = [

        0 => [
            'name' => 'Hoạt động',
            'class' => ''
        ],
        1 => [
            'name' => 'Không hoạt động ',
            'class' => ''
        ]
    ];

    public static function status($status)
    {
        $statusItem = self::$statusText[$status];
        return '<label class="'. $statusItem['class'] .'">'. $statusItem['name'] .'</label>';
    }

}

<?php

namespace Modules\Manage\Enum;

class ServiceTypeEnum
{
    const ELECTRICITY = 0;
    const WATER = 1;
    const OTHER = 3;
    const Network = 4;

    public static $typeService = [
        self::ELECTRICITY => [
            'name' => 'Tiền điện'
        ],
        self::WATER => [
            'name' => 'TIền nước'
        ],
        self::Network => [
            'name' => 'Tiền mạng'
        ],
        self::OTHER => [
            'name' => 'Khác'
        ]
    ];

    public static function typeServiceText($id)
    {

        return '<span>'.self::$typeService[$id]['name'].'</span>';
    }


}

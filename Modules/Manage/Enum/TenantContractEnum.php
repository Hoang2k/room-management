<?php

namespace Modules\Manage\Enum;

class TenantContractEnum
{

    public static $pay_period = [
        1 => [
            'name' => ' 1 tháng'
        ],
        3 => [
            'name' => '3 tháng'
        ],
        6 => [
            'name' => '6 tháng'
        ],
        12 => [
            'name' => '1 năm'
        ]
    ];
}

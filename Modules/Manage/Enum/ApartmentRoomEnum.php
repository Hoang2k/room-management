<?php

namespace Modules\Manage\Enum;

class ApartmentRoomEnum
{

    const NO_ACTIVE = 0;
    const ACTIVE = 1;

    public static $statusText = [

        self::NO_ACTIVE => [
            'name' => 'Đã hết phòng',
            'class' => ''
        ],
        self::ACTIVE => [
            'name' => 'Còn phòng',
            'class' => ''
        ]
    ];

    public static function status($status)
    {
        $statusItem = self::$statusText[$status];
        return '<label class="'. $statusItem['class'] .'">'. $statusItem['name'] .'</label>';
    }
}

<?php

namespace Modules\Manage\Enum;

class PaymentTypeEnum
{

    public static $electricity_pay_type = [
        0 => [
            'name' => 'Trả theo đầu người'
        ],
        1 => [
            'name' =>'Trả cố định theo phòng'
        ],
        2 => [
            'name' => 'Trả theo số tiền sử dụng'
        ]
    ];

    public static $water_pay_type = [
        0 => [
            'name' => 'Trả theo đầu người'
        ],
        1 => [
            'name' =>'Trả cố định theo phòng'
        ],
        2 => [
            'name' => 'Trả theo số nước sử dụng'
        ]
    ];

    public static $pay_period = [

    ];

}

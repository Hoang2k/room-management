<?php

namespace Modules\Manage\Repository;

use App\Repository\RepositoryInterface;

interface ApartmentRepositoryInterface extends RepositoryInterface
{

    public function getList($filter , $paginate);

}

<?php

namespace Modules\Manage\Repository;

use App\Repository\BaseRepository;
use Modules\Manage\Entities\ApartmentRoom;

class RoomRepository extends BaseRepository implements RoomRepositoryInterface
{

    protected $model;

    public function __construct(ApartmentRoom $model)
    {
        $this->model = $model;
    }

    public function getList($filter, $paginate) {
        $model = $this->model;

        if ($filter) {
            foreach ($filter as $fil) {
                foreach ($fil as $key => $val) {
                    $model = $model->where($key, 'like', '%' . $val . '%');
                }
            }
        }
        $model = $model->orderBy('id', 'desc');

        return $paginate
            ? $model->paginate($paginate)
            : $model->get();


    }

}

<?php

namespace Modules\Manage\Repository;

use App\Repository\BaseRepository;
use Modules\Manage\Entities\TenantContract;

class TenantContractRepository extends BaseRepository
{
   public $model;

   public function __construct(TenantContract $model)
   {
       $this->model = $model;
   }
}

<?php

namespace Modules\Manage\Repository;

use App\Repository\BaseRepository;
use Modules\Manage\Entities\Tenant;

class TenantRepository extends BaseRepository
{
    protected $model;

    public function __construct(Tenant $model)
    {
        $this->model = $model;
    }

    public function getList() {
        $model = $this->model;

        return $model->get();
    }
}

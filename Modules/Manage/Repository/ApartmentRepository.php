<?php

namespace Modules\Manage\Repository;

use App\Repository\BaseRepository;
use Modules\Manage\Entities\Apartment;

class ApartmentRepository extends BaseRepository implements ApartmentRepositoryInterface
{
    protected $model;

    public function __construct(Apartment $model)
    {
        $this->model = $model;
    }

    public function getList($filter, $paginate)
    {
        $model = $this->model;

        if ($filter) {
            foreach ($filter as $fil) {
                foreach ($fil as $key => $val) {
                    $model = $model->where($key, 'like', '%' . $val . '%');
                }
            }
        }
       $model = $model->orderBy('id', 'desc');

        return $paginate
            ? $model->paginate($paginate)
            : $model->get();

    }

}

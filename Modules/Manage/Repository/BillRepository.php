<?php

namespace Modules\Manage\Repository;

use App\Repository\BaseRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Modules\Manage\Entities\RoomFeeCollection;

class BillRepository extends BaseRepository implements BillRepositoryInterface
{
    protected $model;


    public function __construct(RoomFeeCollection $model)
    {
        $this->model = $model;
    }

    public function getList($filter, $paginate)
    {
        $model = $this->model;
        $model = $model->orderBy('id', 'desc');

        return $paginate
            ? $model->paginate($paginate)
            : $model->get();
    }

    public function getCustomerDebt()
    {
        $model = $this->model->with('Tenant');

        $model = $model->where('total_debt', '>', 0)
            ->whereMonth('charge_date', '=', Carbon::now()->subMonth()->month)
            ->get();

        return $model;


    }

    //xử lý dữ liệu hiển thị ra biểu đồ theo từng tháng
    public function getDataForChart()
    {
        $model = $this->model;
        $query = $model->select(
            DB::raw('SUM(total_price) as `total_price_month`'),
            DB::raw('SUM(total_debt) as `total_debt_month`'),
            DB::raw('MONTH(charge_date) as month'))
            ->groupBy(DB::raw('MONTH(charge_date)'))
            ->get();

        return $query;
    }

    public function getDataChartByUser($room_id)
    {
        $model = $this->model;
        $query = $model->select(
            DB::raw('SUM(total_price) as `total_price_month`'),
            DB::raw('SUM(total_debt) as `total_debt_month`'),
            DB::raw('MONTH(charge_date) as month'))
            ->whereIn('apartment_room_id', $room_id)
            ->groupBy(DB::raw('MONTH(charge_date)'))
            ->get();

        return $query;
    }

}

<?php

namespace Modules\Manage\Repository;

interface RoomRepositoryInterface
{
    public function getList($filter,$paginate);

}

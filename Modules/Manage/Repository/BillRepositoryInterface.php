<?php

namespace Modules\Manage\Repository;

use App\Repository\RepositoryInterface;

interface BillRepositoryInterface extends RepositoryInterface
{

    public function getList($filter,$paginate);
}

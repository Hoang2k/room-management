<?php

namespace Modules\Manage\Repository\MonthlyCost;

use App\Repository\BaseRepository;
use Modules\Manage\Entities\MonthlyCost;

class MonthlyCostRepository extends BaseRepository implements MonthlyCostRepositoryInterface
{
    protected $model;

    public function __construct(MonthlyCost $model)
    {
        $this->model = $model;
    }

    public function getList($filter, $paginate)
    {
       $model = $this->model;
       $model= $model->orderBy('id','desc');

       return $paginate
              ? $model->paginate($paginate)
              : $model->get();
    }

}

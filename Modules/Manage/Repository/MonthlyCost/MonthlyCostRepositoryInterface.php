<?php

namespace Modules\Manage\Repository\MonthlyCost;

interface MonthlyCostRepositoryInterface
{

    public function getList($filter, $paginate);
}

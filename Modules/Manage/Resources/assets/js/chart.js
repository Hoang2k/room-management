import {Chart, registerables} from "chart.js";

Chart.register(...registerables);
const JsChart = {
    init() {
        this.showChart()
        this.showChartByUser()
    },
    showChart() {
        const ctx = $('.chart-item');
        $.ajax({
            url: "manage/data/chart",
            type: "get",
            success: function (res) {
                if (res.success == true) {
                    let data = res.data;
                    let month = data.columns;
                    let priceRoomByMonth = data.rows.total_price_month;
                    let priceDebtByMonth = data.rows.total_debt_month;
                    const myChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: month,
                            datasets: [
                                {
                                    label: 'Tiền trọ',
                                    data: priceRoomByMonth,
                                    backgroundColor: 'rgba(0, 128, 128, 0.3)',
                                    borderColor: 'rgba(0, 128, 128, 0.7)',
                                    borderWidth: 1
                                },
                                {
                                    label: 'Tiền nợ',
                                    data: priceDebtByMonth,
                                    backgroundColor: 'rgba(0, 128, 128, 0.7)',
                                    borderColor: 'rgba(0, 128, 128, 1)',
                                    borderWidth: 1
                                }
                            ]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true
                                    }
                                }]
                            }
                        }
                    });
                }
            }
        })

    },

    showChartByUser() {
        $('.user-chart').each(function () {
            let user_id = $(this).find('.chart').attr('data-id');
            const ctx = $(`.chart-${user_id}`);
            $.ajax({
                url: "manage/user/data/chart",
                type: "get",
                data: {
                    user_id: user_id
                },
                success: function (res) {
                    if (res.success == true) {
                        let data = res.data;
                        let month = data.columns;
                        let priceRoomByMonth = data.rows.total_price_month;
                        let priceDebtByMonth = data.rows.total_debt_month;
                        const myChart = new Chart(ctx, {
                            type: 'bar',
                            data: {
                                labels: month,
                                datasets: [
                                    {
                                        label: 'Tiền trọ',
                                        data: priceRoomByMonth,
                                        backgroundColor: 'rgba(0, 128, 128, 0.3)',
                                        borderColor: 'rgba(0, 128, 128, 0.7)',
                                        borderWidth: 1
                                    },
                                    {
                                        label: 'Tiền nợ',
                                        data: priceDebtByMonth,
                                        backgroundColor: 'rgba(0, 128, 128, 0.7)',
                                        borderColor: 'rgba(0, 128, 128, 1)',
                                        borderWidth: 1
                                    }
                                ]
                            },
                            options: {
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }]
                                }
                            }
                        });
                    }
                }
            })

        })
    }
}

$(function () {
    JsChart.init()
})

export default JsChart

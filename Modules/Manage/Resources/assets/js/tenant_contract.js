const Tenant_contract = {
    init() {
        this.selectOldTenant()
    },
    selectOldTenant() {
        $('.js-select-tenant').change(function () {
            let id = this.value;
            if (id) {
                $.ajax({
                    url: "manage/tenants/info/" + id,
                    type: 'GET'
                }).done(function (res) {
                    let data = res.data;
                    $('.js-tenant-name').val(data.name)
                    $('.js-tenant-phone').val(data.phone)
                    $('.js-tenant-email').val(data.email)
                })
            } else {
                $('.js-tenant-name').val('')
                $('.js-tenant-phone').val('')
                $('.js-tenant-email').val('')
            }

        })
    }
}

$(function () {
    Tenant_contract.init()
})

export default Tenant_contract

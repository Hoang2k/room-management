@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    {{\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('user')}}
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-2 justify-content-between d-flex">
                        <div class="">
                            <form action="{{route('get.user.index')}}" method="get" class="d-flex ">
                                <div class="form-group">
                                    <input name="name" value="{{Request::has('name')? Request::get('name') : ''}}"
                                           class="form-control" placeholder="Nhập tên" type="text">
                                </div>
                                <div class="form-group pl-2">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Tìm kiếm
                                    </button>
                                    <a href="{{route('get.user.index')}}" class="btn btn-dark"><i class="fa"></i>
                                        Reset</a>
                                </div>
                            </form>
                        </div> <!--end left -->
                    </div>

                    <div class="table-responsive">
                        <div id="products-datatable_wrapper" class="dataTables_wrapper dt-bootstrap5 no-footer">
                            <div class="row ">
                                <div class="col-sm-12">
                                    <table
                                        class="table table-centered w-100 dt-responsive nowrap dataTable no-footer dtr-inline">
                                        <thead class="table-light">
                                        <tr>
                                            <th>ID</th>
                                            <th>Tên</th>
                                            <th>Email</th>
                                            <th>Số lượng tòa nhà</th>
                                            <th align="center">Số lượng phòng</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($users as $user)
                                            <tr>
                                                <td>{{$user->id}}</td>
                                                <td>{{$user->name}}</td>
                                                <td>{{$user->email}}</td>
                                                <td>{{$user->Apartment->count()}}</td>
                                                <td align="center">{{$user->ApartmentRoom->count()}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-7">
                                     {{$users->links()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col -->
    </div>
@endsection

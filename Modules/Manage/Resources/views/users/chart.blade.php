@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <h3>Biểu đồ thống kê tiền trọ hàng tháng</h3>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        @foreach($users as $user)
            <div class="col-md-12 user-chart">
                <h4>{{$user->name}}</h4>
                <div class="row">
                    <div class="col-md-5">
                        <div class="">
                            <h4><strong>Doanh thu (VNĐ)</strong></h4>
                        </div>
                        <canvas id="chart-item" data-id="{{$user->id}}" class="chart chart-{{$user->id}}"></canvas>
                    </div>
                </div>

            </div>

        @endforeach

    </div>
@endsection
@section('js')
    <script src="{{ mix('vendor/js/manage.js') }}"></script>
@endsection

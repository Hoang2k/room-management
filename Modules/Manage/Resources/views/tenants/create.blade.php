@extends('layouts.master')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    {{\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('tenants::add')}}
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @include('manage::tenants.form',['action' => route('get.tenant.store',['id'=> $id])])
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col -->
    </div>
@endsection

<form method="post" action="{{$action}}" enctype="multipart/form-data">
    @csrf
    <input type="hidden"  name="room_id" value="{{$id}}">
    <div class="form-group">
        <label>Tên <span class="text-danger">(*)</span></label>
        <input type="text" class="form-control" value="{{isset($item) ? $item->name : ''}}" name="name" placeholder="Nhập tên ">
        @error('name')
        <span class="text text-danger ">{{ $message }}</span>
        @enderror
    </div>
    <div class="form-group">
        <label>Số điện thoại  <span class="text-danger">(*)</span></label>
        <input class="form-control" type="tel" name="phone" value="{{isset($item) ? $item->phone : ''}}" placeholder="Nhập số điện thoại">
        @error('phone')
        <span class="text text-danger ">{{ $message }}</span>
        @enderror
    </div>
    <div class="form-group">
        <label for="">Số CMT <span class="text text-danger">(*)</span></label>
        <input class="form-control" value="{{old('identity_card_number',isset($item) ? $item->identity_card_number : '')}}" type="text"
               placeholder="Số chứng minh thư" name="identity_card_number">
        @error('identity_card_number')
        <span class="text text-danger ">{{ $message }}</span>
        @enderror
    </div>
    <div class="form-group">
        <label for="">Email <span class="text text-danger">(*)</span></label>
        <input class="form-control" value="{{old('email',isset($item) ? $item->email : '')}}" type="text"
               placeholder="Nhập Email" name="email">
        @error('email')
        <span class="text text-danger ">{{ $message }}</span>
        @enderror
    </div>
    <div class="form-group">
        @if(isset($item))
            <button type="submit" class="btn btn-primary">Cập nhật</button>
        @else
            <button type="submit" class="btn btn-success">Thêm mới</button>
        @endif
        <button type="reset" class="btn btn-dark">Hủy bỏ</button>
    </div>

</form>
@section('js')
    <script src="{{ mix('vendor/js/manage.js') }}"></script>
@endsection

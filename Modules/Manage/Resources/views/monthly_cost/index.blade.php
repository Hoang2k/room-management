@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    {{\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('monthly_cost')}}
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <p>
                    <strong>Lưu ý: <span class="text-danger">(*)</span> </strong>
                    <br>
                    Các dịch vụ phải được gán cho từng khách thuê phòng để khi tính tiền sẽ có tiền dịch vụ đó.
                </p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-2 justify-content-between d-flex">
                        <div class="">
                            <form action="{{route('monthly.cost.index')}}" method="get" class="d-flex ">
                                <div class="form-group">
                                    <input name="name" value="{{Request::has('name')? Request::get('name') : ''}}"
                                           class="form-control" placeholder="Nhập tên" type="text">
                                </div>
                                <div class="form-group pl-2">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Tìm kiếm
                                    </button>
                                    <a href="{{route('monthly.cost.index')}}" class="btn btn-dark"><i class="fa"></i>
                                        Reset</a>
                                </div>
                            </form>
                        </div> <!--end left -->
                        <div class="">
                            <a href="{{route('monthly.cost.create')}}" class="btn btn-sm btn-primary">Thêm mới</a>
                        </div>

                    </div>

                    <div class="table-responsive">
                        <div id="products-datatable_wrapper" class="dataTables_wrapper dt-bootstrap5 no-footer">
                            <div class="row ">
                                <div class="col-sm-12">
                                    <table
                                        class="table table-centered w-100 dt-responsive nowrap dataTable no-footer dtr-inline">
                                        <thead class="table-light">
                                        <tr>
                                            <th>ID</th>
                                            <th>Tên</th>
                                            <th>Loại dịch vụ</th>
                                            <th>Đơn giá (VNĐ)</th>
                                            <th align="center">Tác vụ</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($items as $item)
                                            <tr>
                                                <td>{{$item->id}}</td>
                                                <td>{{$item->name}}</td>
                                                <td>{!! \Modules\Manage\Enum\ServiceTypeEnum::typeServiceText($item->type_service) !!}</td>
                                                <td>{{number_format($item->unitPrice) }}</td>
                                                <td width="10%">
                                                    <a href="{{route('monthly.cost.edit',['id' =>$item->id])}}" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i>
                                                        Sửa</a>
                                                    <a  href="{{route('monthly.cost.destroy',['id' =>$item->id])}}" class="btn btn-sm btn-danger "><i class="fa fa-trash"></i> Xóa</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-7">
                                    {{$items->links()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col -->
    </div>
@endsection

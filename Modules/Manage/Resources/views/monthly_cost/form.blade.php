<form method="post" action="{{$action}}" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>Tên <span class="text-danger">(*)</span></label>
        <input type="text" name="name" value="{{old('name', isset($item) ? $item->name  : '')}}" class="form-control"
               placeholder="Tên dịch vụ">
        @error('name')
        <span class="text text-danger ">{{ $message }}</span>
        @enderror
    </div>
    <div class="form-group">
        <label>Loại dịch vụ <span class="text-danger">(*)</span></label>
        <select class="form-control" name="type_service" id="">
            <option value="">--Loại dịch vụ--</option>
            @foreach($type_service as $key => $type_service)
                <option {{isset($item) && $item->type_service == $key ? 'selected' : ''}} value="{{$key}}">{{$type_service['name']}}</option>
            @endforeach
        </select>
        @error('type_service')
        <span class="text text-danger ">{{ $message }}</span>
        @enderror
    </div>
    <div class="form-group">
        <label>Đơn giá <span class="text-danger">(*)</span></label>
        <input name="unitPrice" value="{{old('unitPrice', isset($item) ? $item->unitPrice  : '')}}" type="number"
               class="form-control" placeholder="Nhập đơn giá">
        @error('unitPrice')
        <span class="text text-danger ">{{ $message }}</span>
        @enderror
    </div>

    <div class="form-group">
        <label>Mô tả</label>
        <textarea name="description" class="form-control">{{isset($item) ? $item->description : ''}}
</textarea>

    </div>
    <div class="form-group">
        @if(isset($item))
            <button type="submit" class="btn btn-primary">Cập nhật</button>
        @else
            <button type="submit" class="btn btn-success">Thêm mới</button>
        @endif
        <button type="reset" class="btn btn-dark">Hủy bỏ</button>
    </div>

</form>
@section('js')
    <script src="{{ mix('vendor/js/manage.js') }}"></script>
@endsection

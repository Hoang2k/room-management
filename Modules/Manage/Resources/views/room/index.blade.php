@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    {{\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('apartment_room')}}
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-2 justify-content-between d-flex">
                        <div class="">
                            <form action="{{route('get.room.index')}}" method="get" class="d-flex ">
                                <div class="form-group">
                                    <select class="form-control" name="apartment_id" id="">
                                        <option value="">--Chọn tòa nhà--</option>
                                        @foreach($apartments as $apartment)
                                            <option
                                                {{Request::get('apartment_id')== $apartment->id ? 'selected' : ''}} value="{{$apartment->id}}">{{$apartment->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group pl-1">
                                    <input name="room_number"
                                           value="{{Request::has('room_number') ? Request::get('room_number') : ''}}"
                                           class="form-control" type="text" placeholder="Số phòng">
                                </div>
                                <div class="form-group pl-2">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Tìm kiếm
                                    </button>
                                    <a href="{{route('get.room.index')}}" class="btn btn-dark"><i class="fa"></i>
                                        Reset</a>
                                </div>
                            </form>
                        </div> <!--end left -->
                        <div class="">
                            <a href="{{route('get.room.create')}}" class="btn btn-sm btn-primary">Thêm mới</a>
                        </div>

                    </div>

                    <div class="table-responsive">
                        <div id="products-datatable_wrapper" class="dataTables_wrapper dt-bootstrap5 no-footer">
                            <div class="row ">
                                <div class="col-sm-12">
                                    <table
                                        class="table table-centered w-100 dt-responsive nowrap dataTable no-footer dtr-inline">
                                        <thead class="table-light">
                                        <tr>
                                            <th>ID</th>
                                            <th>Tên tòa nhà</th>
                                            <th>Hình ảnh</th>
                                            <th>Số phòng</th>
                                            <th>Giá thuê</th>
                                            <th>Người thuê</th>
                                            <th>Số người đang ở</th>
                                            <th>Trạng thái</th>
                                            <th>Thời gian</th>
                                            <th class="text-center">Tác vụ</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($items as $item)
                                            <tr>
                                                <td>{{$item->id}}</td>
                                                <td>{{$item->Apartment->name}}</td>
                                                <td>
                                                    <img src="{{$item->image}}" width="80px" alt="{{$item->image}}">
                                                </td>
                                                <td>{{number_format( $item->room_number)}} </td>
                                                <td>{{$item->room_price}}</td>
                                                <td>
                                                    <i class="fa fa-user"></i>
                                                    @if($item->TenantContract)
                                                        <a class="text-info"
                                                           href="{{route('get.tenant.edit',['id'=>$item->TenantContract->id])}}">
                                                            {{$item->Tenant->name}}
                                                        </a>
                                                    @endif

                                                </td>
                                                <td>{{$item->max_tenant}} </td>
                                                <td>{!! \Modules\Manage\Enum\ApartmentRoomEnum::status($item->status) !!}</td>
                                                <td>{{$item->created_at}}</td>
                                                <td align="center">
                                                        <a href="{{route('tenant_contract.create',['id'=> $item->id])}}"
                                                           class="btn btn-sm btn-warning">
                                                            <i class="fa fa-book"></i>
                                                            Hợp đồng
                                                        </a>
                                                    <a class="btn btn-sm btn-primary"
                                                       href="{{route('get.room.edit',['id'=>$item->id])}}"><i
                                                            class="fa fa-edit"></i>
                                                        Sửa</a>
                                                    <a onclick="confirm('Bạn có chắc chắn muốn xóa')"
                                                       class="btn btn-sm btn-danger"
                                                       href="{{route('get.room.destroy',['id'=>$item->id])}}">
                                                        <i class="fa fa-trash"></i>
                                                        Xóa</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-7">

                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col -->
    </div>
@endsection

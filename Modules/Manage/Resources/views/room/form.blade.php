<form method="post" action="{{$action}}" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>Tòa nhà <span class="text-danger">(*)</span></label>
        <select name="apartment_id" class="form-control" id="">
            @foreach($apartments as $apartment)
                <option value="{{$apartment->id}}">{{$apartment->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label>Hình ảnh </label>
        <input id="inpImg" type="file" class="form-control">
        <div id="image_show" class="mt-2">
            @isset($item)
                <a href="{{ $item->image }}" target="_blank">
                    <img src="{{ $item->image }}" width="100px">
                </a>
            @endisset
        </div>
        <input type="hidden" name="image" value="{{isset($item) ? $item->image : ''}}" id="thumb">
    </div>
    <div class="form-group">
        <label for="">Số phòng <span class="text text-danger">(*)</span></label>
        <input class="form-control" value="{{old('room_number',isset($item) ? $item->room_number : '')}}" type="text"
               placeholder="Số phòng" name="room_number">
        @error('room_number')
        <span class="text text-danger ">{{ $message }}</span>
        @enderror
    </div>
    <div class="form-group">
        <label for="">Số người </label>
        <input class="form-control" value="{{old('max_tenant',isset($item) ? $item->max_tenant : '')}}" type="text"
               placeholder="Số người" name="max_tenant">
    </div>
    <div class="form-group">
        <label>Giá phòng </label>
        <input name="room_price" value="{{old('room_price', isset($item) ? $item->room_price  : '')}}" type="text"
               class="form-control" placeholder="Nhập giá phòng">
        @error('room_price')
        <span class="text text-danger ">{{ $message }}</span>
        @enderror
    </div>
    <div class="form-group">
        <label for="">Trạng thái <span class="text-danger">(*)</span> </label>
        <select class="form-control" name="status" id="">
            <option value="">-- Chọn trạng thái--</option>
            @foreach($status as $key => $status)
                <option
                    {{isset($item) ?? $item->status == $key ? 'selected' : ''}} value="{{$key}}">{{$status['name']}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label>Mô tả</label>
        <textarea name="description" class="form-control">{{isset($item) ? $item->description : ''}}
</textarea>

    </div>
    <div class="form-group">
        @if(isset($item))
            <button type="submit" class="btn btn-primary">Cập nhật</button>
        @else
            <button type="submit" class="btn btn-success">Thêm mới</button>
        @endif
        <button type="reset" class="btn btn-dark">Hủy bỏ</button>
    </div>

</form>
@section('js')
    <script src="{{ mix('vendor/js/manage.js') }}"></script>
@endsection

<form method="post" action="{{$action}}" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>Tên <span class="text-danger">(*)</span></label>
        <input type="text" name="name" value="{{old('name', isset($item) ? $item->name  : '')}}" class="form-control"
               placeholder="Nhập tên toà nhà">
        @error('name')
        <span class="text text-danger ">{{ $message }}</span>
        @enderror
    </div>
    <div class="form-group">
        <label>Hình ảnh <span class="text-danger">(*)</span></label>
        <input id="inpImg" type="file" class="form-control">
        <div id="image_show" class="mt-2">
            @isset($item)
                <a href="{{ $item->image }}" target="_blank">
                    <img src="{{ $item->image }}" width="100px">
                </a>
            @endisset
        </div>
        <input type="hidden" name="image" id="thumb">
    </div>
    <div class="form-group">
        <label>Chủ nhà <span class="text-danger">(*)</span></label>
        <select class="form-control" name="user_id" id="">
            @foreach($users as $user)
                <option value="{{$user->id}}">{{$user->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label>Địa chỉ <span class="text-danger">(*)</span></label>
        <input name="address" value="{{old('address', isset($item) ? $item->address  : '')}}" type="text"
               class="form-control" placeholder="Nhập địa chỉ">
        @error('address')
        <span class="text text-danger ">{{ $message }}</span>
        @enderror
    </div>
    <div class="form-group">
        <label for="">Trạng thái <span class="text-danger">(*)</span> </label>
        <select class="form-control" name="status" id="">
            <option value="">-- Chọn trạng thái--</option>
            @foreach($status as $key => $status)
                <option
                    {{isset($item) ?? $item->status == $key ? 'selected' : ''}} value="{{$key}}">{{$status['name']}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label>Mô tả</label>
        <textarea name="description" class="form-control">{{isset($item) ?? $item->description}}
</textarea>

    </div>
    <div class="form-group">
        @if(isset($item))
            <button type="submit" class="btn btn-primary">Cập nhật</button>
        @else
            <button type="submit" class="btn btn-success">Thêm mới</button>
        @endif
        <button type="reset" class="btn btn-dark">Hủy bỏ</button>
    </div>

</form>
@section('js')
    <script src="{{ mix('vendor/js/manage.js') }}"></script>
@endsection

@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    {{\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('apartment')}}
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-2 justify-content-between d-flex">
                        <div class="">
                            <form action="{{route('get.apartment.index')}}" method="get" class="d-flex ">
                                <div class="form-group">
                                    <input name="name" value="{{Request::has('name')? Request::get('name') : ''}}"
                                           class="form-control" placeholder="Nhập tên" type="text">
                                </div>
                                <div class="form-group pl-1">
                                    <input name="address"
                                           value="{{Request::has('address') ? Request::get('address') : ''}}"
                                           class="form-control" type="text" placeholder="Địa chỉ">
                                </div>
                                <div class="form-group pl-2">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Tìm kiếm
                                    </button>
                                    <a href="{{route('get.apartment.index')}}" class="btn btn-dark"><i class="fa"></i>
                                        Reset</a>
                                </div>
                            </form>
                        </div> <!--end left -->
                        <div class="">
                            <a href="{{route('get.apartment.create')}}" class="btn btn-sm btn-primary">Thêm mới</a>
                        </div>

                    </div>

                    <div class="table-responsive">
                        <div id="products-datatable_wrapper" class="dataTables_wrapper dt-bootstrap5 no-footer">
                            <div class="row ">
                                <div class="col-sm-12">
                                    <table
                                        class="table table-centered w-100 dt-responsive nowrap dataTable no-footer dtr-inline">
                                        <thead class="table-light">
                                        <tr>
                                            <th>ID</th>
                                            <th>Tên</th>
                                            <th>Hình ảnh</th>
                                            <th>Chủ nhà</th>
                                            <th>Địa chỉ</th>
                                            <th>Trạng thái</th>
                                            <th>Thời gian</th>
                                            <th>Tác vụ</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($items as $item)
                                            <tr>
                                                <td>{{$item->id}}</td>
                                                <td>{{$item->name}}</td>
                                                <td>
                                                    <img src="{{$item->image}}" width="80px" alt="{{$item->image}}">
                                                </td>
                                                <td>{{$item->user_id}}
                                                <td>{{$item->address}}</td>
                                                <td> {!! \Modules\Manage\Enum\ApartmentStatusEnum::status($item->status) !!}</td>
                                                <td>{{$item->created_at}}</td>
                                                <td>
                                                    <a class="btn btn-sm btn-primary"
                                                       href="{{route('get.apartment.edit',['id' => $item->id])}}"><i
                                                            class="fa fa-edit"></i>
                                                        Sửa</a>
                                                    <a onclick="confirm('Bạn có chắc chắn muốn xóa')"
                                                       class="btn btn-sm btn-danger"
                                                       href="{{route('get.apartment.destroy',['id' => $item->id])}}">
                                                        <i class="fa fa-trash"></i>
                                                        Xóa</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-7">
                                    {{$items->links()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col -->
    </div>
@endsection

<form method="post" action="{{$action}}" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-md-6">
            <input type="hidden" name="id" value="{{isset($item) ?? $item->id}}" >
            <div class="form-group d-flex">
                <label class="col-md-2">Tên <span class="text-danger">(*)</span></label>
                <div class="col-md-10">
                    <input type="text" class="form-control js-tenant-name" value="{{isset($item) ? $item->Tenant->name : ''}}"
                           name="name" placeholder="Nhập tên ">
                    @error('name')
                    <span class="text text-danger ">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group d-flex">
                <label class="col-md-2">Lấy khách cũ</label>
                <div class="col-md-10">
                    <select name="tenant_id" class="form-control js-select-tenant">
                        <option value="">--Chọn khách cũ--</option>
                        @foreach($tenants as $tenant)
                            <option value="{{$tenant->id}}">{{$tenant->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group d-flex">
                <label class="col-md-2 d-flex">Điện thoại <span class="text-danger">(*)</span></label>
                <div class="col-md-10">
                    <input type="number" class="form-control js-tenant-phone"
                           value="{{isset($item) ? $item->Tenant->phone : ''}}" name="phone" placeholder="Nhập số điện thoại ">
                    @error('phone')
                    <span class="text text-danger ">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group d-flex">
                <label class="col-md-2">Email <span class="text-danger">(*)</span></label>
                <div class="col-md-10">
                    <input type="email" class="form-control js-tenant-email"
                           value="{{isset($item) ? $item->Tenant->email : ''}}" name="email" placeholder="Nhập email ">
                    @error('email')
                    <span class="text text-danger ">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group d-flex">
                <label class="col-md-2">Giá phòng <span class="text-danger">(*)</span></label>
                <div class="col-md-10">
                    <input type="text" class="form-control" value="{{isset($item) ? $item->price : ''}}" name="price"
                           placeholder="Nhập giá phòng ">
                    @error('price')
                    <span class="text text-danger ">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group d-flex">
                <label class="col-md-2">Cách trả tiền điện <span class="text-danger">(*)</span></label>
                <div class="col-md-10">
                    <select class="form-control" name="electricity_pay_type" id="">
                        <option value="">--Chọn cách thức--</option>
                        @foreach($electricity_pay_type as $key => $electricity_pay_type)
                            <option value="{{$key}}">{{$electricity_pay_type['name']}}</option>
                        @endforeach
                    </select>
                    @error('electricity_pay_type')
                    <span class="text text-danger ">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group d-flex">
                <label class="col-md-2">Giá điện <span class="text-danger">(*)</span></label>
                <div class="col-md-10">
                    <input type="number" class="form-control" value="{{isset($item) ? $item->electricity_price : ''}}"
                           name="electricity_price" placeholder="Nhập giá điện ">
                    @error('electricity_price')
                    <span class="text text-danger ">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group d-flex">
                <label class="col-md-2">Số điện bắt đầu <span class="text-danger">(*)</span></label>
                <div class="col-md-10">
                    <input type="text" class="form-control"
                           value="{{isset($item) ? $item->electricity_number_start : ''}}"
                           name="electricity_number_start" placeholder="Nhập số điện bắt đàu">
                    @error('electricity_number_start')
                    <span class="text text-danger ">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group d-flex">
                <label class="col-md-2">Số người <span class="text-danger">(*)</span></label>
                <div class="col-md-10">
                    <input type="number" class="form-control"
                           value="{{isset($item) ? $item->number_of_tenant_current : ''}}"
                           name="number_of_tenant_current" placeholder="Nhập số người ">
                    @error('number_of_tenant_current')
                    <span class="text text-danger ">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group d-flex">
                <label class="col-md-2">Ngày bắt đầu <span class="text-danger">(*)</span></label>
                <div class="col-md-10">
                    <input type="date" class="form-control" value="{{isset($item) ? date($item->start_time) : ''}}"
                           name="start_time">
                    @error('start_time')
                    <span class="text text-danger ">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group d-flex">
                <label class="col-md-2">Ngày kết thúc <span class="text-danger">(*)</span></label>
                <div class="col-md-10">
                    <input type="date" class="form-control" value="{{isset($item) ? $item->end_time : ''}}"
                           name="end_time">
                    @error('end_time')
                    <span class="text text-danger ">{{ $message }}</span>
                    @enderror
                </div>
            </div>
        </div>  <!--end col left -->
        <div class="col-md-6">
            <div class="form-group d-flex">
                <label class="col-md-2">Cách trả tiền nước <span class="text-danger">(*)</span></label>
                <div class="col-md-10">
                    <select class="form-control" name="water_pay_type" id="">
                        <option value="">--Chọn cách thức--</option>
                        @foreach($water_pay_type as $key=> $water_pay_type)
                            <option value="{{$key}}">{{$water_pay_type['name']}}</option>
                        @endforeach
                    </select>
                    @error('water_pay_type')
                    <span class="text text-danger ">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group d-flex">
                <label class="col-md-2 d-flex">Giá nước <span class="text-danger">(*)</span></label>
                <div class="col-md-10">
                    <input type="text" class="form-control" value="{{isset($item) ? $item->water_price : ''}}"
                           name="water_price" placeholder="Nhập giá nước ">
                    @error('water_price')
                    <span class="text text-danger ">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group d-flex">
                <label class="col-md-2">Số nước bắt đầu <span class="text-danger">(*)</span></label>
                <div class="col-md-10">
                    <input type="text" class="form-control" value="{{isset($item) ? $item->water_number_start : ''}}"
                           name="water_number_start" placeholder="Số nước bắt đầu ">
                    @error('water_number_start')
                    <span class="text text-danger ">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group d-flex">
                <label class="col-md-2">Kỳ hạn thanh toán <span class="text-danger">(*)</span></label>
                <div class="col-md-10">
                    <select class="form-control" name="pay_period" id="">
                        <option value="">--Chọn kỳ hạn</option>
                        @foreach($pay_periods as $key => $pay_period)
                        <option value="{{$key}}">{{$pay_period['name']}}</option>
                        @endforeach
                    </select>

                    @error('pay_period')
                    <span class="text text-danger ">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group d-flex">
                <label class="col-md-2">Ghi chú khác </label>
                <div class="col-md-10">
                    <textarea name="note" class="form-control" rows="3"></textarea>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="apartment_room_id" value="{{$apartment_room_id}}">
    <div class="form-group">
        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Lưu</button>
        <a href="{{route('get.room.index')}}" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Quay về</a>
    </div>

</form>
@section('js')
    <script src="{{ mix('vendor/js/manage.js') }}"></script>
@endsection

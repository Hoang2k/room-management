@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <h3>Biểu đồ thống kê tiền trọ hàng tháng</h3>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-5">
                    <div class="">
                        <h4><strong>Doanh thu (VNĐ)</strong></h4>
                    </div>
                    <canvas id="chart-item" class="chart-item"></canvas>
                </div>
            </div>

        </div>
    </div>
@endsection
@section('js')
    <script src="{{ mix('vendor/js/manage.js') }}"></script>
@endsection

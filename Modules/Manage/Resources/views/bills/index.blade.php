@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    {{\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('bills')}}
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-2 justify-content-between d-flex">
                        <div class="">
                            <form action="{{route('get.bill.index')}}" method="get" class="d-flex ">
                                <div class="form-group">
                                    <select class="form-control" name="apartment_id" id="">
                                        <option value="">--Chọn Ngày--</option>
                                    </select>
                                </div>
                                <div class="form-group pl-1">
                                    <input name="room_number"
                                           value="{{Request::has('room_number') ? Request::get('room_number') : ''}}"
                                           class="form-control" type="text" placeholder="Số phòng">
                                </div>
                                <div class="form-group pl-2">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Tìm kiếm
                                    </button>
                                    <a href="{{route('get.bill.index')}}" class="btn btn-dark"><i class="fa"></i>
                                        Reset</a>
                                </div>
                            </form>
                        </div> <!--end left -->
                        <div class="">
                            <a href="{{route('get.bill.create')}}" class="btn btn-sm btn-primary">Tính tiền</a>
                        </div>

                    </div>

                    <div class="table-responsive">
                        <div id="products-datatable_wrapper" class="dataTables_wrapper dt-bootstrap5 no-footer">
                            <div class="row ">
                                <div class="col-sm-12">
                                    <table
                                        class="table table-centered w-100 dt-responsive nowrap dataTable no-footer dtr-inline">
                                        <thead class="table-light">
                                        <tr>
                                            <th>ID</th>
                                            <th>Tên tòa nhà</th>
                                            <th>Số phòng</th>
                                            <th>Tên khách</th>
                                            <th>Số điện</th>
                                            <th>Số nước (Khối)</th>
                                            <th>Thời gian thanh toán</th>
                                            <th>Số tiền (VNĐ)</th>
                                            <th>Đã trả (VNĐ)</th>
                                            <th>Còn lại (VNĐ)</th>
                                            <th class="text-center">Tác vụ</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($items as $item)
                                            <tr>
                                                <td>{{$item->id}}</td>
                                                <td>{{$item->Apartment->name}}</td>
                                                <td>
                                                    {{$item->Room->room_number}}
                                                </td>
                                                <td>
                                                    {{$item->Tenant->name}}
                                                </td>
                                                <td>
                                                    {{$item->electricity_number}}
                                                </td>
                                                <td>
                                                    {{$item->water_number}}
                                                </td>
                                                <td>
                                                    {{$item->charge_date}}
                                                </td>
                                                <td>
                                                    {{$item->total_price}}
                                                </td>
                                                <td>{{$item->total_paid}}
                                                <td>
                                                    {{$item->total_debt}}
                                                </td>
                                                <td align="center">
                                                    <a href=""
                                                       class="btn btn-sm btn-info">
                                                        <i class="fa fa-eye"></i>
                                                        Xem
                                                    </a>
                                                    <a class="btn btn-sm btn-primary"
                                                       href="{{route('get.bill.edit',['id'=>$item->id])}}"><i
                                                            class="fa fa-edit"></i>
                                                        Sửa</a>
                                                    <a onclick="confirm('Bạn có chắc chắn muốn xóa')"
                                                       class="btn btn-sm btn-danger"
                                                       href="{{route('get.bill.destroy',['id'=> $item->id])}}">
                                                        <i class="fa fa-trash"></i>
                                                        Xóa</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-7">

                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col -->
    </div>
@endsection

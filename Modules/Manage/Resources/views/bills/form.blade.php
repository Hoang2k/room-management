<form method="post" action="{{$action}}" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>Tòa nhà <span class="text-danger">(*)</span></label>
        <select name="apartment_id" class="form-control" id="">
            @foreach($apartments as $apartment)
                <option
                    {{isset($item) ?? $item->apartment_id == $apartment->id ? 'selected' : ''}} value="{{$apartment->id}}">{{$apartment->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label>Phòng <span class="text-danger">(*)</span> </label>
        <select class="form-control" name="apartment_room_id" id="">
            @foreach($apartment_rooms as $apartment_room)
                <option
                    {{isset($item) ?? $item->apartment_room_id == $apartment_room->id ? 'selected' : ''}} value="{{$apartment_room->id}}">{{$apartment_room->room_number}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="">Tên khách <span class="text text-danger">(*)</span></label>
        <select class="form-control" name="tenant_id" id="">
            @foreach($tenants as $tenant)
                <option
                    {{isset($item) ?? $item->$tenant_id == $tenant->id ? 'selected' : ''}}   value="{{$tenant->id}}">{{$tenant->name}}</option>
            @endforeach
        </select>
        @error('tenant_id')
        <span class="text text-danger ">{{ $message }}</span>
        @enderror
    </div>
    <div class="form-group">
        <label for="">Số điện sử dụng</label>
        <input value="{{old('electricity_number',isset($item) ? $item->electricity_number : '')}}" class="form-control"
               name="electricity_number" type="number" placeholder="Số điện sử dụng">
        @error('electricity_number')
        <span class="text text-danger ">{{ $message }}</span>
        @enderror
    </div>
    <div class="form-group">
        <label for="">Số nước sử dụng</label>
        <input value="{{old('water_number',isset($item) ? $item->water_number : '')}}" class="form-control"
               name="water_number" type="number" placeholder="Số nước sử dụng">
        @error('water_number')
        <span class="text text-danger ">{{ $message }}</span>
        @enderror
    </div>

    <div class="form-group">
        <label for="">Ngày thanh toán <span class="text text-danger">(*)</span></label>
        <input value="{{old('charge_date',isset($item) ? $item->charge_date : '')}}" class="form-control"
               name="charge_date" type="date">
    </div>
    <div class="form-group">
        <label for="">Số tiền cần thanh toán </label>
        <input value="{{old('total_price',isset($item) ? $item->total_price : '')}}" class="form-control"
               value="{{old('total_price',isset($item) ? $item->total_price : '')}}" type="number"
               placeholder="Số tiền cần thanh toán" name="total_price">
    </div>
    <div class="form-group">
        <label>Số tiền đã thanh toán </label>
        <input name="total_paid" value="{{old('total_paid', isset($item) ? $item->total_paid  : '')}}" type="number"
               class="form-control" placeholder="Số tiền đã thanh toán">
        @error('total_paid')
        <span class="text text-danger ">{{ $message }}</span>
        @enderror
    </div>

    <div class="form-group">
        @if(isset($item))
            <button type="submit" class="btn btn-primary">Cập nhật</button>
        @else
            <button type="submit" class="btn btn-success">Thêm mới</button>
        @endif
        <button type="reset" class="btn btn-dark">Hủy bỏ</button>
    </div>

</form>
@section('js')
    <script src="{{ mix('vendor/js/manage.js') }}"></script>
@endsection

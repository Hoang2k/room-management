<?php

namespace Modules\Manage\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Manage\Http\Requests\CreateBillRequest;
use Modules\Manage\Http\Requests\CreateTenantRequest;
use Modules\Manage\Services\ApartmentService;
use Modules\Manage\Services\BillService;
use Modules\Manage\Services\RoomService;
use Modules\Manage\Services\TenantService;

class BillController extends Controller
{
    protected $billService;
    protected $apartmentService;
    protected $roomService;
    protected $tenantService;

    public function __construct(
        BillService      $billService,
        ApartmentService $apartmentService,
        RoomService      $roomService,
        TenantService    $tenantService
    )
    {
        $this->billService = $billService;
        $this->apartmentService = $apartmentService;
        $this->roomService = $roomService;
        $this->tenantService = $tenantService;
    }

    public function index()
    {
        $items = $this->billService->getList();

        $viewData = [
            'items' => $items
        ];

        return view('manage::bills.index')->with($viewData);
    }


    public function create()
    {
        $apartments = $this->apartmentService->getList();
        $apartment_rooms = $this->roomService->getList();
        $tenants = $this->tenantService->getList();

        $viewData = [
            'apartments' => $apartments,
            'apartment_rooms' => $apartment_rooms,
            'tenants' => $tenants
        ];

        return view('manage::bills.create')->with($viewData);
    }


    public function store(CreateBillRequest $request)
    {
        $data = $request->except('_token');
        $this->billService->insert($data);

        return redirect()->route('get.bill.index');
    }


    public function edit($id)
    {
        $item = $this->billService->findById($id);
        $apartments = $this->apartmentService->getList();
        $apartment_rooms = $this->roomService->getList();
        $tenants = $this->tenantService->getList();

        $viewData = [
            'item' => $item,
            'apartments' => $apartments,
            'apartment_rooms' => $apartment_rooms,
            'tenants' => $tenants
        ];

        return view('manage::bills.edit')->with($viewData);
    }

    public function update(Request $request, $id)
    {
        $data = $request->except('_token');
        $this->billService->update($id, $data);

        return redirect()->route('get.bill.index');

    }


    public function destroy($id)
    {
        $this->billService->delete($id);

        return redirect()->back();
    }
}

<?php

namespace Modules\Manage\Http\Controllers;

use App\Models\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Manage\Services\ApartmentService;
use Modules\Manage\Services\BillService;

class AdminController extends Controller
{
    protected $apartmentService;
    protected $billService;

    public function __construct(
        ApartmentService $apartmentService,
        BillService      $billService
    )
    {
        $this->apartmentService = $apartmentService;
        $this->billService = $billService;
    }

    public function getUser()
    {
        $users = $this->apartmentService->getUser();
        $viewData = [
            'users' => $users
        ];

        return view('manage::users.index')->with($viewData);
    }


    public function getChart()
    {
        $users = $this->apartmentService->getUser();
        $viewData = [
            'users' => $users
        ];


        return view('manage::users.chart')->with($viewData);
    }

    public function getDataForChart(Request $request)
    {
        $room_id = [];
        $user_id = $request->get('user_id');
        $apartment_room = User::find($user_id)->ApartmentRoom;
        if ($apartment_room->count() > 0) {
            foreach ($apartment_room as $room) {
                $room_id[] = $room->id;
            }

        }
        $dataTrans = $this->billService->getDataChartByUser($room_id);

        return response()->json([
            'success' => true,
            'data' => $dataTrans
        ]);


    }


}

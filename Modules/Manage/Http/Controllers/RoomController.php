<?php

namespace Modules\Manage\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Manage\Enum\ApartmentRoomEnum;
use Modules\Manage\Enum\ApartmentStatusEnum;
use Modules\Manage\Events\LogHistory;
use Modules\Manage\Http\Requests\CreateRoomRequest;
use Modules\Manage\Services\ApartmentService;
use Modules\Manage\Services\RoomService;

class RoomController extends Controller
{
    protected $apartmentService;
    protected $roomService;

    public function __construct(
        ApartmentService $apartmentService,
        RoomService      $roomService
    )
    {
        $this->apartmentService = $apartmentService;
        $this->roomService = $roomService;
    }


    public function index(Request $request)
    {
        $filter = [];
        $apartment_id = $request->get('apartment_id');
        $room_number = $request->get('room_number');

        if ($apartment_id || $room_number) {
            $filter = [
                [
                    'apartment_id' => $apartment_id
                ],
                [
                    'room_number' => $room_number
                ]
            ];

        }
        $apartments = $this->apartmentService->getList();
        $items = $this->roomService->getList($filter);
        $viewData = [
            'items' => $items,
            'apartments' => $apartments
        ];

        return view('manage::room.index')->with($viewData);
    }


    public function create()
    {
        $status = ApartmentRoomEnum::$statusText;
        $apartments = $this->apartmentService->getList();
        $viewData = [
            'status' => $status,
            'apartments' => $apartments
        ];

        return view('manage::room.create')->with($viewData);

    }


    public function store(CreateRoomRequest $request)
    {
        $data = $request->except(['_token']);

        $this->roomService->insert($data);
        event(new LogHistory($data,'create'));

        return redirect()->route('get.room.index');
    }


    public function show($id)
    {
        return view('manage::show');
    }

    public function edit($id)
    {
        $item = $this->roomService->findById($id);
        $status = ApartmentStatusEnum::$statusText;
        $apartments = $this->apartmentService->getList();
        $viewData = [
            'item' => $item,
            'apartments' => $apartments,
            'status' => $status
        ];

        return view('manage::room.edit')->with($viewData);
    }


    public function update(CreateRoomRequest $request, $id)
    {
        $data = $request->except(['_token', 'redirect']);
        $this->roomService->update($data, $id);

        return redirect()->route('get.room.index');


    }

    public function destroy($id)
    {
        $this->roomService->delete($id);

        return redirect()->back();
    }
}

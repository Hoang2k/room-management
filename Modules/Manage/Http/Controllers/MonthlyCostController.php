<?php

namespace Modules\Manage\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Manage\Enum\ServiceTypeEnum;
use Modules\Manage\Http\Requests\MonthlyCostRequest;
use Modules\Manage\Services\MonthlyCostService;

class MonthlyCostController extends Controller
{
    protected $monthlyCostService;

    public function __construct(MonthlyCostService $monthlyCostService)
    {
        $this->monthlyCostService = $monthlyCostService;
    }

    public function index()
    {
        $items = $this->monthlyCostService->getList();
        $viewData = [
            'items' => $items
        ];
        return view('manage::monthly_cost.index')->with($viewData);
    }

    public function create()
    {
         $type_service = ServiceTypeEnum::$typeService;
         $viewData = [
             'type_service' => $type_service
         ];

        return view('manage::.monthly_cost.create')->with($viewData);
    }


    public function store(MonthlyCostRequest $request)
    {
        $data = $request->except('_token');
        $this->monthlyCostService->insert($data);

        return redirect()->route('monthly.cost.index');
    }


    public function show($id)
    {
        return view('manage::show');
    }

    public function edit($id)
    {
        $item = $this->monthlyCostService->findById($id);
        $type_service = ServiceTypeEnum::$typeService;
        $viewData = [
            'type_service' => $type_service,
            'item' => $item
        ];

        return view('manage::monthly_cost.edit')->with($viewData);
    }


    public function update(MonthlyCostRequest $request, $id)
    {
        $data = $request->except(['_token', 'redirect']);
        $this->monthlyCostService->update($data, $id);

        return redirect()->route('monthly.cost.index');
    }


    public function destroy($id)
    {
        $this->monthlyCostService->delete($id);

        return redirect()->back();

    }
}

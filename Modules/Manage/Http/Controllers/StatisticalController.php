<?php

namespace Modules\Manage\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Manage\Entities\ApartmentRoom;
use Modules\Manage\Services\BillService;
use Modules\Manage\Services\RoomService;

class StatisticalController extends Controller
{

    protected $roomService;
    protected $billService;

    public function __construct(
        RoomService $roomService,
        BillService $billService
    )
    {
        $this->roomService = $roomService;
        $this->billService = $billService;
    }

    public function index()
    {
        $rooms = $this->roomService->getList();

        $viewData = [
            'rooms' => $rooms
        ];

        return view('manage::statistical.index')->with($viewData);
    }


    public function getDataForChart()
    {
       $data = $this->billService->getDataForChart();

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }


    public function edit($id)
    {
        return view('manage::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace Modules\Manage\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Manage\Entities\TenantContract;
use Modules\Manage\Enum\PaymentTypeEnum;
use Modules\Manage\Enum\TenantContractEnum;
use Modules\Manage\Http\Requests\ContractRequest;
use Modules\Manage\Services\TenantContractService;
use Modules\Manage\Services\TenantService;

class TenantContractController extends Controller
{
    public $tenantService;
    protected $tenantContractService;

    public function __construct(
        TenantService         $tenantService,
        TenantContractService $tenantContractService

    )
    {
        $this->tenantService = $tenantService;
        $this->tenantContractService = $tenantContractService;
    }

    public function index()
    {
        return view('');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create($id)
    {
        $apartment_room_id = $id;
        $tenants = $this->tenantService->getList();
        $tenant_contract = TenantContract::where('apartment_room_id', $apartment_room_id)->first();
        $viewData = [
            'tenants' => $tenants,
            'apartment_room_id' => $apartment_room_id,
            'electricity_pay_type' => PaymentTypeEnum::$electricity_pay_type,
            'water_pay_type' => PaymentTypeEnum::$water_pay_type,
            'pay_periods' => TenantContractEnum::$pay_period,
            'item' => $tenant_contract
        ];

        return view('manage::contract.create')->with($viewData);
    }


    public function storeOrUpdate(ContractRequest $request)
    {
        $id = $request->get('id');
        if ($id) {
            $this->update($request,$id);
        }
        else
        {
            $this->tenantContractService->insert($request);
        }

        return redirect()->route('get.room.index');


    }

    public function update($request, $id)
    {
       return $this->tenantContractService->update($request,$id);
    }

}

<?php

namespace Modules\Manage\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Manage\Http\Requests\CreateTenantRequest;
use Modules\Manage\Services\TenantService;

class TenantController extends Controller
{
    protected $tenantService;

    public function __construct(TenantService $tenantService)
    {
        $this->tenantService = $tenantService;
    }

    public function index()
    {
        return view('manage::index');
    }


    public function create(Request $request)
    {
        $id = $request->get('id');

        return view('manage::tenants.create', [
            'id' => $id
        ]);
    }

    public function store(CreateTenantRequest $request)
    {
        $data = $request->except('_token');
        $this->tenantService->insert($data);

        return redirect()->route('get.room.index');
    }


    public function show($id)
    {
        $item = $this->tenantService->findById($id);

        return response()->json([
            'success' => true,
            'data'    =>$item
        ]);
    }


    public function edit($id)
    {
        $item = $this->tenantService->findById($id);
        $viewData = [
            'item' => $item
        ];

        return view('manage::tenants.edit')->with($viewData);
    }


    public function update(CreateTenantRequest $request, $id)
    {
        $data = $request->except(['_token']);
        $this->tenantService->update($data, $id);

        return redirect()->route('get.room.index');
    }


    public function destroy($id)
    {
        //
    }
}

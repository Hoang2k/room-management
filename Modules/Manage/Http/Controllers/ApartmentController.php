<?php

namespace Modules\Manage\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Manage\Enum\ApartmentStatusEnum;
use Modules\Manage\Events\LogHistory;
use Modules\Manage\Http\Requests\CreateApartmentRequest;
use Modules\Manage\Services\ApartmentService;

class ApartmentController extends Controller
{

    protected $apartmentService;

    public function __construct(ApartmentService $apartmentService)
    {
        $this->apartmentService = $apartmentService;
    }


    public function index(Request $request)
    {
        $filter = [];
        if($request->get('name') || $request->get('address')) {
            $filter = [
                [
                    'name' => $request->get('name')
                ],
                [
                    'address' => $request->get('address')
                ]
            ];

        }

        $items = $this->apartmentService->getList($filter);
        $viewData = [
            'items'  => $items,

        ];

        return view('manage::pages.index')->with($viewData);
    }


    public function create()
    {
        $users = $this->apartmentService->getUser();
        $status = ApartmentStatusEnum::$statusText;

        $viewData = [
            'users' => $users,
            'status' => $status
        ];


        return view('manage::pages.create')->with($viewData);
    }

    public function store(CreateApartmentRequest $request)
    {
        $data = $request->except(['_token']);
        $this->apartmentService->insert($data);
        event(new LogHistory($data,'create'));

        return redirect()->route('get.apartment.index');
    }

    public function show($id)
    {
        return view('manage::show');
    }

    public function edit($id)
    {
        $item = $this->apartmentService->findById($id);
        $status = ApartmentStatusEnum::$statusText;
        $users = $this->apartmentService->getUser();
        $viewData = [
            'item' => $item,
            'users' => $users,
            'status' => $status
        ];

        return view('manage::pages.edit')->with($viewData);
    }

    public function update(CreateApartmentRequest $request, $id)
    {
        $data = $request->except(['_token', 'redirect']);
        $this->apartmentService->update($data, $id);

        return redirect()->route('get.apartment.index');

    }

    public function destroy($id)
    {
        $this->apartmentService->delete($id);

        return redirect()->back();
    }
}

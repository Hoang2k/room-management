<?php

namespace Modules\Manage\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateBillRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'electricity_number' => 'required',
            'water_number' => 'required',
            'total_paid' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'electricity_number.required' => 'Số điện sử dụng không được để trống',
            'water_number.required' => 'Số nước sử dụng không được để trống',
            'total_paid.required' => 'Số tiền đã thanh toán không được để trống'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}

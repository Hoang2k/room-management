<?php

namespace Modules\Manage\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContractRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'price' => 'required',
            'electricity_pay_type' => 'required',
            'electricity_price' => 'required',
            'electricity_number_start' => 'required',
            'number_of_tenant_current' => 'required',
            'start_time' => 'required',
            'water_pay_type' => 'required',

        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Không được để trống trường này',
            'phone.required' => 'Không được để trống trường này',
            'email.required' => 'Không được để trống trường này',
            'price.required' => 'Không được để trống trường này',
            'electricity_pay_type.required' => 'Không được để trống trường này',
            'electricity_price.required' => 'Không được để trống trường này',
            'electricity_number_start.required' => 'Không được để trống trường này',
            'number_of_tenant_current.required' => 'Không được để trống trường này',
            'start_time.required' => 'Không được để trống trường này',
            'water_pay_type.required' => 'Không được để trống trường này',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}

<?php

namespace Modules\Manage\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MonthlyCostRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'type_service' => 'required',
            'unitPrice' => 'required',
        ];
    }

    public function messages()
    {
       return [
           'name.required' => 'Không được để trống trường này',
           'type_service.required' => 'Không được để trống trường này',
           'unitPrice.required' => 'Không được để trống trường này',
       ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}

<?php

namespace Modules\Manage\Services;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Modules\Manage\Repository\TenantRepository;


class TenantService
{

    protected $tenantRepository;

    public function __construct(TenantRepository $tenantRepository)
    {
        $this->tenantRepository = $tenantRepository;
    }

    public function getList() {

        return $this->tenantRepository->getList();

    }
    public function insert($data)
    {
        try {
            $result = $this->tenantRepository->create($data);
            Session::flash('success', 'Thêm thành công');
        } catch (\Exception $err) {
            Session::flash('error', 'Thêm thất bại');
            Log::info($err->getMessage());

            return false;
        }

        return $result;
    }

    public function findById($id)
    {

        return $this->tenantRepository->find($id);
    }

    public function update($data, $id)
    {
        try {
            $this->tenantRepository->update($id, $data);
            Session::flash('success', 'Cập nhật thành công');
        } catch (\Exception $err) {
            Session::flash('error', 'Cập nhật thất bại');
            Log::info($err->getMessage());

            return false;
        }

        return true;
    }

}

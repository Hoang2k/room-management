<?php

namespace Modules\Manage\Services;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Modules\Manage\Entities\RoomFeeCollection;
use Modules\Manage\Repository\BillRepository;

class BillService
{

    protected $billRepository;

    public function __construct(BillRepository $billRepository)
    {
        $this->billRepository = $billRepository;
    }

    public function getList($filter = [])
    {

        return $this->billRepository->getList($filter, $paginate = 20);
    }

    public function insert($data)
    {
        $total_price = $data['total_price'];
        $total_paid = $data['total_paid'];
        $total_debt = (int)($total_price) - (int)($total_paid);
        if ($total_debt <= 0) {
            $total_debt = 0;
        } else {
            $total_debt = $total_debt;
        }
        $data['total_debt'] = $total_debt;
        try {
            $this->billRepository->create($data);
            Session::flash('success', 'Thêm thành công');
        } catch (\Exception $err) {
            Session::flash('error', 'Thêm thất bại');
            Log::info($err->getMessage());

            return false;
        }

        return true;
    }

    public function findById($id)
    {

        return $this->billRepository->find($id);
    }

    public function update($id, $data)
    {
        $total_price = $data['total_price'];
        $total_paid = $data['total_paid'];
        $total_debt = (int)($total_price) - (int)($total_paid);
        if ($total_debt <= 0) {
            $total_debt = 0;
        } else {
            $total_debt = $total_debt;
        }
        $data['total_debt'] = $total_debt;
        try {
            $this->billRepository->update($id, $data);
            Session::flash('success', 'Cập nhật thành công');
        } catch (\Exception $err) {
            Session::flash('error', 'Cập nhật thất bại');
            Log::info($err->getMessage());

            return false;
        }

        return true;
    }

    public function delete($id)
    {

        return $this->billRepository->delete($id);
    }

    public function getCustomerDebt()
    {

        return $this->billRepository->getCustomerDebt();
    }

    public function getDataForChart()
    {
        $data = $this->billRepository->getDataForChart();
        $dataTrans = [];
        foreach ($data as $item) {
            $dataTrans['columns'][] = 'Tháng ' . $item->month;
            $dataTrans['rows']['total_price_month'][] = $item->total_price_month;
            $dataTrans['rows']['total_debt_month'][] = $item->total_debt_month;

        }

        return $dataTrans;
    }

    public function getDataChartByUser($room_id)
    {
        //Nếu user có phòng thì xử lý dữ liệu
        if ($room_id) {
            $data = $this->billRepository->getDataChartByUser($room_id);
            $dataTrans = [];
            foreach ($data as $item) {
                $dataTrans['columns'][] = 'Tháng ' . $item->month;
                $dataTrans['rows']['total_price_month'][] = $item->total_price_month;
                $dataTrans['rows']['total_debt_month'][] = $item->total_debt_month;

            }
        } //Nếu không có phòng gán dữ liệu bằng 0
        else {
            $dataTrans['columns'][] = 'Tháng ';
            $dataTrans['rows']['total_price_month'][] = 0;
            $dataTrans['rows']['total_debt_month'][] = 0;
        }


        return $dataTrans;
    }
}

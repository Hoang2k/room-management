<?php

namespace Modules\Manage\Services;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Modules\Manage\Enum\ApartmentRoomEnum;
use Modules\Manage\Repository\RoomRepository;

class RoomService
{

    protected $roomRepository;
    public function __construct(RoomRepository $roomRepository)
    {
        $this->roomRepository = $roomRepository;
    }

    public function getList($filter=[]) {

        return $this->roomRepository->getList($filter = $filter,$paginate=10);
    }

    public function insert($data) {
        try {
            $this->roomRepository->create($data);

            Session::flash('success','Thêm thành công');
        }
        catch (\Exception $err) {
            Session::flash('error','Thêm thất bại');
            Log::info($err->getMessage());

            return false;
        }

        return true;
    }

    public function findById($id) {

        return $this->roomRepository->find($id);
    }

    public function update($data,$id) {
        try {
            $this->roomRepository->update($id,$data);
            Session::flash('success','Cập nhật thành công');
        }
        catch (\Exception $err) {
            Session::flash('error','Cập nhật thất bại');
            Log::info($err->getMessage());

            return false;
        }

        return true;
    }

    public function delete($id) {

        return $this->roomRepository->delete($id);
    }
}

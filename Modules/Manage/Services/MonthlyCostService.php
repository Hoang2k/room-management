<?php

namespace Modules\Manage\Services;

use Illuminate\Support\Facades\Log;
use Modules\Manage\Repository\MonthlyCost\MonthlyCostRepository;
use Modules\Manage\Repository\MonthlyCost\MonthlyCostRepositoryInterface;

class MonthlyCostService
{

    protected $monthlyCostRepository;

    public function __construct(MonthlyCostRepository $monthlyCostRepository)
    {
        $this->monthlyCostRepository = $monthlyCostRepository;
    }

    public function getList() {

        return $this->monthlyCostRepository->getList($filter = false, $paginate = 20);
    }

    public function insert(array $data) {

        try {
            $this->monthlyCostRepository->create($data);
        }
        catch (\Exception $err) {
            Log::info($err->getMessage());

            return false;
        }

        return true;
    }

    public function update($data,$id) {
        try {
            $this->monthlyCostRepository->update($id,$data);
        }
        catch (\Exception $err) {
            Log::info($err->getMessage());

            return false;
        }

        return true;
    }

    public function findById($id) {

        return $this->monthlyCostRepository->find($id);
    }

    public function delete($id) {

        return $this->monthlyCostRepository->delete($id);
    }
}

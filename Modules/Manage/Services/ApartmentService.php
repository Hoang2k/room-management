<?php

namespace Modules\Manage\Services;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Modules\Manage\Repository\ApartmentRepository;
use phpDocumentor\Reflection\PseudoTypes\False_;

class ApartmentService
{

    protected $apartmentRepository;

    public function __construct(ApartmentRepository $apartmentRepository)
    {
        $this->apartmentRepository = $apartmentRepository;
    }

    public function getList($filter= []) {

     return $this->apartmentRepository->getList($filter,$paginate = 20);

    }

    public function insert($data) {

        try {
            $this->apartmentRepository->create($data);

            Session::flash('success','Thêm thành công');
        }
        catch (\Exception $err) {
            Session::flash('error','Thêm thất bại');
            Log::info($err->getMessage());

            return false;
        }

        return true;
    }

    public function update($data,$id) {

        try {
             $this->apartmentRepository->update($id,$data);
             Session::flash('success','Cập nhật thành công');
        }
        catch (\Exception $err) {
            Session::flash('error','Cập nhật thất bại');
            Log::info($err->getMessage());

            return false;
        }

        return true;
    }

    public function delete($id) {

        return $this->apartmentRepository->delete($id);
    }

    public function findById($id) {

        return $this->apartmentRepository->find($id);
    }


    public function getUser() {

        return User::with('Apartment')->paginate(20);
    }

}

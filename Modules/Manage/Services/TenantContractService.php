<?php

namespace Modules\Manage\Services;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Modules\Manage\Repository\TenantContractRepository;

class TenantContractService
{

    protected $tenantContractRepository;
    protected $tenantService;

    public function __construct(
        TenantContractRepository $tenantContractRepository,
        TenantService $tenantService

    )
    {
        $this->tenantContractRepository = $tenantContractRepository;
        $this->tenantService = $tenantService;
    }

    public function insert($request) {
        $data = $request->all();
        $dataContract = Arr::except($data,['_token','name','phone','email','id']);
        if ($request->get('tenant_id')) {
            try {
                $this->tenantContractRepository->create($dataContract);
            }
            catch (\Exception $err) {
                Log::info($err->getMessage());

                return false;
            }
        }
        else {
            $dataTenant['name'] = $request->get('name');
            $dataTenant['email'] = $request->get('email');
            $dataTenant['phone'] = $request->get('phone');
            $dataTenant['room_id'] = $request->get('apartment_room_id');
            $idTenant = $this->tenantService->insert($dataTenant)->id;
            $dataContract['tenant_id'] = $idTenant;
            $this->tenantContractRepository->create($dataContract);
        }

        return true;

    }

    public function update($request,$id) {
        $data = $request->all();
        $dataContract = Arr::except($data,['_token','name','phone','email','id']);
        if ($request->get('tenant_id')) {
            try {
                $this->tenantContractRepository->update($id,$dataContract);
            }
            catch (\Exception $err) {
                Log::info($err->getMessage());

                return false;
            }
        }
        else {
            $dataTenant['name'] = $request->get('name');
            $dataTenant['email'] = $request->get('email');
            $dataTenant['phone'] = $request->get('phone');
            $dataTenant['room_id'] = $request->get('apartment_room_id');
            $idTenant = $this->tenantService->insert($dataTenant)->id;
            $dataContract['tenant_id'] = $idTenant;
            $this->tenantContractRepository->update($id,$dataContract);
        }

        return true;
    }

}

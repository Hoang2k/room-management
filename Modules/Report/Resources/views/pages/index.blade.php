@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    {{\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('customer_debt')}}
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-2 justify-content-between d-flex">
                        <div class="">
                            <form action="{{route('get.bill.index')}}" method="get" class="d-flex ">
                                <div class="form-group">
                                    <select class="form-control" name="apartment_id" id="">
                                        <option value="">--Chọn Ngày--</option>
                                    </select>
                                </div>
                                <div class="form-group pl-1">
                                    <input name="room_number"
                                           value="{{Request::has('room_number') ? Request::get('room_number') : ''}}"
                                           class="form-control" type="text" placeholder="Số phòng">
                                </div>
                                <div class="form-group pl-2">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Tìm kiếm
                                    </button>
                                    <a href="{{route('get.bill.index')}}" class="btn btn-dark"><i class="fa"></i>
                                        Reset</a>
                                </div>
                            </form>
                        </div> <!--end left -->
                        <div class="">
                            <a href="{{route('get.bill.create')}}" class="btn btn-sm btn-primary">Tính tiền</a>
                        </div>

                    </div>

                    <div class="table-responsive">
                        <div id="products-datatable_wrapper" class="dataTables_wrapper dt-bootstrap5 no-footer">
                            <div class="row ">
                                <div class="col-sm-12">
                                    <table
                                        class="table table-centered w-100 dt-responsive nowrap dataTable no-footer dtr-inline">
                                        <thead class="table-light">
                                        <tr>
                                            <th>ID</th>
                                            <th>Tên khách</th>
                                            <th>Số điện thoại</th>
                                            <th>Email</th>
                                            <th>Tên tòa nhà</th>
                                            <th>Số phòng</th>
                                            <th>Số tiền (VNĐ)</th>
                                            <th>Đã trả (VNĐ)</th>
                                            <th>Còn lại (VNĐ)</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($customer_debts as $item)
                                            <tr>
                                                <td>{{$item->id}}</td>

                                                <td>
                                                    {{$item->Tenant->name}}

                                                <td>
                                                    {{$item->Tenant->phone}}
                                                </td>
                                                <td>
                                                    {{$item->Tenant->email}}
                                                </td>
                                                <td>{{$item->Apartment->name}}</td>
                                                <td>
                                                    {{$item->Room->room_number}}
                                                </td>
                                                <td>
                                                    {{$item->total_price}}
                                                </td>
                                                <td>{{$item->total_paid}}
                                                <td>
                                                    {{$item->total_debt}}
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-7">

                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col -->
    </div>
@endsection

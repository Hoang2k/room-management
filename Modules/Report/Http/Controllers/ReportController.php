<?php

namespace Modules\Report\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Manage\Services\BillService;
use Modules\Report\Services\ReportService;

class ReportController extends Controller
{
    protected $reportService;
    protected $billService;


    public function __construct(
        ReportService $reportService,
        BillService   $billService
    )
    {
        $this->reportService = $reportService;
        $this->billService = $billService;
    }

    public function index()
    {
        $customer_debts = $this->billService->getCustomerDebt();

        $viewData = [
            'customer_debts' => $customer_debts
        ];

        return view('report::pages.index')->with($viewData);
    }

}

# apartment
php artisan migrate --path=Modules/Manage/Database/Migrations/2022_07_26_082232_create_apartments_table.php
#apartment_rooms
php artisan migrate --path=Modules/Manage/Database/Migrations/2022_07_28_091236_create_apartment_rooms_table.php
#tenants
php artisan migrate --path=Modules/Manage/Database/Migrations/2022_07_29_022613_create_tenants_table.php

#Room_Fee_Collection
php artisan migrate --path=Modules/Manage/Database/Migrations/2022_07_29_063415_create_room_fee_collections_table.php
